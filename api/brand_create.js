(function() {
    var Brands = require('../data_access/brands').Brands;

    function createBrand(rqst, res, next, helpers) {
                var brand_name = rqst.body.brand_name;
                var created_date = rqst.body.created_date;
        Brands.createBrands({brand_name,created_date}, {}, helpers, function(response) {
            res.json(response)
        })
    }

    function getBrand(rqst, res, next, helpers) {                  
        Brands.getBrands({}, {}, helpers, function(response) {
            res.json(response)
        })
    }
    exports.createBrand = createBrand;
    exports.getBrand = getBrand;
})()